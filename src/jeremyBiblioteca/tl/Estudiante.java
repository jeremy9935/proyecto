/*
atributos que el estudiante lleva 

fecah de creacion 17/6/2019
*/
package jeremyBiblioteca.tl;

public class Estudiante extends Profesor {
    
    private String carrera;
    private int nunCreditos;

    public Estudiante() {
        super();
    }

    public Estudiante(String nombre, String apellido, int cedula,String carrera, int nunCreditos) {
        super(nombre,apellido,cedula);
        this.carrera = carrera;
        this.nunCreditos = nunCreditos;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public int getNunCreditos() {
        return nunCreditos;
    }

    public void setNunCreditos(int nunCreditos) {
        this.nunCreditos = nunCreditos;
    }

    @Override
    public String toString() {
        return "Estudiante{" + super.toString()+ "carrera=" + carrera + ", nunCreditos=" + nunCreditos + '}';
    }
    
    
    
    
    
    
}
