/*
los datos que el administrador tiene 

fecha de creacion de la clase 17/6/2019
*/
package jeremyBiblioteca.tl;

public class Administrador {
    
    protected String nombre;
    protected String apellido;
    protected int cedula;
    private char nombramiento;
    private int horasSemanales;

    public Administrador() {
        
    }
    
    public Administrador(String nombre, String apellido, int cedula, char nombramiento, int horasSemanales) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.nombramiento = nombramiento;
        this.horasSemanales = horasSemanales;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public char getNombramiento() {
        return nombramiento;
    }

    public void setNombramiento(char nombramiento) {
        this.nombramiento = nombramiento;
    }

    public int getHorasSemanales() {
        return horasSemanales;
    }

    public void setHorasSemanales(int horasSemanales) {
        this.horasSemanales = horasSemanales;
    }

    @Override
    public String toString() {
        return "Administrador{" + "nombre=" + nombre + ", apellido=" + apellido + ", cedula=" + cedula + ", nombramiento=" + nombramiento + ", horasSemanales=" + horasSemanales + '}';
    }
    
    
    
    
}
